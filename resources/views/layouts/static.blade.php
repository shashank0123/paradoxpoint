<!doctype html>
<html lang="en-US" class="no-js">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Paradox-Point</title>
	<script type="text/javascript">
		window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/stories.star-themes.com\/wellness\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.13"}};
		!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
	</script>
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>
	<link rel='stylesheet' href='static/css/stylesaead.css?ver=5.0.3' type='text/css' media='all' />
	<link rel='stylesheet' href='static/css/js_composer.min5243.css?ver=5.4.5' type='text/css' media='all' />
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins%3A200%2C200i%2C300%2C300i%2C400%2C400i%2C600%2C700%2C800%2C900&amp;ver=4.9.13' type='text/css' media='all' />
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Montserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CVarela+Round%3Aregular&amp;ver=1.0' type='text/css' media='all' />
	<link rel='stylesheet' href='static/css/stylec8f9.css?ver=4.9.13' type='text/css' media='all' />
	<link rel='stylesheet' href='static/css/story-style5152.css?ver=1.0' type='text/css' media='all' />
	<script type='text/javascript' src='static/js/jqueryb8ff.js?ver=1.12.4'></script>
	<script type='text/javascript' src='static/js/jquery-migrate.min330a.js?ver=1.4.1'></script>
	{{-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-3174134-79"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-3174134-79');
	</script>
	--}}
	<!--[if lte IE 9]>
		<link rel="stylesheet" type="text/css" href="https://stories.star-themes.com/wellness/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><style type="text/css" data-type="vc_shortcodes-custom-css">
		.vc_custom_1553521287008{margin-top: 40px !important;margin-bottom: -300px !important;}.vc_custom_1552470132106{margin-right: -15px !important;}.vc_custom_1552657493355{padding-top: 20% !important;padding-right: 15% !important;padding-bottom: 25% !important;padding-left: 15% !important;}.vc_custom_1552657512948{padding-top: 25% !important;padding-right: 15% !important;padding-bottom: 20% !important;padding-left: 15% !important;}.vc_custom_1551102936421{margin-left: -15px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>		</head>
		<style>
			@import url('https://fonts.googleapis.com/css?family=Dosis|Poppins');


			body.device-mode {
				background-color: #EFEFEF;
			}
			body.device-mode #storyBlock, body.device-mode #storyButton {
				display: none;	
			}
			#storyBlock {
				display: none;
				padding: 10px 130px 10px 20px;
				cursor: pointer;

				position: fixed;
				right: -400px;
				bottom: 28px;
				z-index: 998;

				background: #292728;
				border-radius: 6px;

				line-height: 1.1;

				-webkit-box-shadow: 0 0 12px 0 rgba(0,0,0,.7);
				-moz-box-shadow: 0 0 12px 0 rgba(0,0,0,.7);
				box-shadow: 0 0 12px 0 rgba(0,0,0,.7);

				-webkit-transition: all .3s ease-in-out; 
				-moz-transition: all .3s ease-in-out; 
				-o-transition: all .3s ease-in-out; 
				-ms-transition: all .3s ease-in-out;
			}

			#storyBlock.open {
				right: 10px;
			}

			#storyBlock .title {
				font-family: 'Poppins';
				font-size: 20px;
				font-weight: 600;
				color: #fff;
				margin-bottom: 7px;
			}

			#storyBlock .text {
				font-family: 'Poppins';
				font-size: 14px;
				color: #fff;
			}

			#storyButton {
				display: none;
				position: fixed;
				bottom: 20px;
				right: -200px;
				z-index: 999;
				cursor: pointer;

				line-height: 1.1;

				-webkit-transition: all .3s ease-in-out; 
				-moz-transition: all .3s ease-in-out; 
				-o-transition: all .3s ease-in-out; 
				-ms-transition: all .3s ease-in-out;
			}

			#storyButton.animate {
				right: 20px;
			}

			#chat-first-border {
				display: block;
				position: relative;
				z-index: 2;
				background: -webkit-linear-gradient(left top, #C38FF5 0%, #C38FF5 100%);
				width: 86px;
				height: 86px;
				border-radius: 50%;

				-webkit-box-shadow: 0 0 12px 0 rgba(0,0,0,.7);
				-moz-box-shadow: 0 0 12px 0 rgba(0,0,0,.7);
				box-shadow: 0 0 12px 0 rgba(0,0,0,.7);
			}

			#chat-second-border {
				position: absolute;
				z-index: 3;
				display: block;
				top: 4px;
				left: 4px;
				width: 78px;
				height: 78px;
				border-radius: 50%;
				background-color: #fff;
				color: #FFF;
				font-size: 50px;
				text-align: center;
				line-height: 78px;
			}
			#chat-content { 
				display: block;
				width: 70px;
				height: 70px;

				position: absolute;
				z-index: 4;
				top: 8px;
				left: 8px;

				background: #bbb;
				background-size: 80px 80px;

				-webkit-border-radius: 50%;
				-moz-border-radius: 50%;
				border-radius: 50%;

				color: #fff;
				font-family: Arial Black;
				font-size: 60px;
				text-align: center;
				line-height: 58px;
			}

			#chat-content i { 
				position: absolute;
				top: 0;
				left: 0;
				z-index: 9;
				width: 17px;
				height: 24px;
				-webkit-border-radius: 4px/3px;
				-moz-border-radius: 4px/3px;
				border-radius: 4px/3px;
				background-color: #adafb5;
				background-image: -webkit-linear-gradient(bottom, #ff1919, #ff5959);
				background-image: -moz-linear-gradient(bottom, #ff1919, #ff5959);
				background-image: -o-linear-gradient(bottom, #ff1919, #ff5959);
				background-image: -ms-linear-gradient(bottom, #ff1919, #ff5959);
				background-image: linear-gradient(to top, #ff1919, #ff5959);
				display: block;
				color: #fff;
				font-style: normal;
				font-family: 'Helvetica';
				font-weight: bold;
				font-size: 16px;
				text-align: center;
				line-height: 28px;
				box-shadow: 0px 1px 3px #000;
				-moz-box-shadow: 0px 1px 3px #000;
				-webkit-box-shadow: 0px 1px 3px #000;
				border: 1px solid rgba(255, 0, 0, .5);
				margin: -10px 0 0 -8px;
				text-shadow: 0 2px 0 #c51414;
			}

			#chat-content img {
				width: 45px;
			}

			#storyButton.open #chat-content i {
				display: none;
			}

			@media all and (min-width: 560px) {
				#storyBlock, #storyButton {
					display: block;
				}
			}

			@-webkit-keyframes rotating /* Safari and Chrome */ {
				from {
					-webkit-transform: rotate(0deg);
					-o-transform: rotate(0deg);
					transform: rotate(0deg);
				}
				to {
					-webkit-transform: rotate(360deg);
					-o-transform: rotate(360deg);
					transform: rotate(360deg);
				}
			}
			@keyframes rotating {
				from {
					-ms-transform: rotate(0deg);
					-moz-transform: rotate(0deg);
					-webkit-transform: rotate(0deg);
					-o-transform: rotate(0deg);
					transform: rotate(0deg);
				}
				to {
					-ms-transform: rotate(360deg);
					-moz-transform: rotate(360deg);
					-webkit-transform: rotate(360deg);
					-o-transform: rotate(360deg);
					transform: rotate(360deg);
				}
			}


			.rotating {
				-webkit-animation: rotating 2s linear;
				-moz-animation: rotating 2s linear;
				-ms-animation: rotating 2s linear;
				-o-animation: rotating 2s linear;
				animation: rotating 2s linear;
			}


			.phone-cont {
				width: 414px;
				height: 780px;
				position: absolute;
				left: 50%;
				top: -15px;
				z-index: 99999;
				-webkit-transform: translateX(-50%) scale(0.8);
				-moz-transform: translateX(-50%) scale(0.8);
				-ms-transform: translateX(-50%) scale(0.8);
				-o-transform: translateX(-50%) scale(0.8);
				transform: translateX(-50%) scale(0.8);
			/* phone */ }
			.phone-cont .phone {
				top: -15px;
				width: 100%;
				height: calc(100% + 30px);
				position: relative; }
				.phone-cont .phone.grey:before, 
				.phone-cont .phone.grey:after,
				.phone-cont .phone.grey .phone-bottom,
				.phone-cont .phone.grey .phone-top {
					background-color: #d5d5d5;
					border-color: #d5d5d5; }
					.phone-cont .phone.grey .screen {
						border-color: #d5d5d5; }
						.phone-cont .phone.grey .home, .phone-cont .phone.grey .speaker {
							border-color: #b9b8b8; }
							.phone-cont .phone:before {
								content: '';
								display: block;
								position: absolute;
								width: 10px;
								top: 60px;
								bottom: 60px;
								left: 0;
								border-left: 2px solid #ccc;
								background: #fff; }
								.phone-cont .phone:after {
									content: '';
									display: block;
									position: absolute;
									width: 10px;
									top: 60px;
									bottom: 60px;
									right: 0;
									border-right: 2px solid #ccc;
									background: #fff; }
									.phone-cont .phone .phone-shadow {
										position: absolute;
										left: 0;
										right: 0;
										top: 0;
										bottom: 0;
										border-radius: 30px;
										-webkit-box-shadow: 13px 9px 23px -5px rgba(0, 0, 0, 0.25);
										-moz-box-shadow: 13px 9px 23px -5px rgba(0, 0, 0, 0.25);
										box-shadow: 13px 9px 23px -5px rgba(0, 0, 0, 0.25); }
										.phone-cont .phone .phone-top {
											position: absolute;
											left: 0;
											right: 0;
											top: 0;
											border-top-left-radius: 30px;
											border-top-right-radius: 30px;
											border: 2px solid #ccc;
											border-bottom: 0;
											height: 60px;
											background: #fff; }
											.phone-cont .phone .phone-bottom {
												position: absolute;
												left: 0;
												right: 0;
												bottom: 0;
												border-bottom-left-radius: 30px;
												border-bottom-right-radius: 30px;
												border: 2px solid #ccc;
												border-top: 0;
												height: 60px;
												background: #fff; }
												.phone-cont .phone .screen {
													border: 1px solid #ccc;
													position: absolute;
													top: 58px;
													bottom: 58px;
													left: 10px;
													right: 10px;
													background-size: cover; }
													.phone-cont .phone .screen::before, .phone-cont .phone .screen::after {
														content: "";
														display: block;
														position: absolute;
														left: 0;
														right: 0;
														background-size: 100%;
														height: 50%;
														background-repeat: no-repeat; }
														.phone-cont .phone .screen::before {
															background-image: url('../img/stories/screen-blank-top.html');
															top: 0;
															background-position: top; }
															.phone-cont .phone .screen::after {
																background-image: url('../img/stories/screen-blank-bottom.html');
																bottom: 0;
																background-position: bottom; }
																.phone-cont .phone .home {
																	width: 36px;
																	height: 36px;
																	border: 1px solid #ccc;
																	position: absolute;
																	bottom: 10px;
																	left: 50%;
																	-webkit-transform: translateX(-50%);
																	-moz-transform: translateX(-50%);
																	-ms-transform: translateX(-50%);
																	-o-transform: translateX(-50%);
																	transform: translateX(-50%);
																	border-radius: 50%; }
																	.phone-cont .phone .speaker {
																		width: 50px;
																		height: 6px;
																		border: 1px solid #ccc;
																		border-radius: 6px;
																		position: absolute;
																		left: 50%;
																		top: 25px;
																		-webkit-transform: translateX(-50%);
																		-moz-transform: translateX(-50%);
																		-ms-transform: translateX(-50%);
																		-o-transform: translateX(-50%);
																		transform: translateX(-50%); }

																		.loader{
																			position: absolute;
																			left: 0;
																			right: 0;
																			bottom: 0;
																			top: 0;
																			text-align: center;
																			z-index: 98;
																		}
																		.loader .fa-spinner{
																			position: absolute;
																			color: #999;
																			top: 50%;
																			font-size: 30px;
																			margin-top: -15px;
																		}

																		.loader .spinner {
																			width: 60px;
																			height: 60px;
																			background-color: #444;
																			position: absolute;
																			top: calc(50% - 210px);
																			left:calc(50% - 30px);
																			border-radius: 100%;  
																			-webkit-animation: sk-scaleout 1.0s infinite ease-in-out;
																			animation: sk-scaleout 1.0s infinite ease-in-out;
																		}

																		@-webkit-keyframes sk-scaleout {
																			0% { -webkit-transform: scale(0) }
																			100% {
																				-webkit-transform: scale(1.0);
																				opacity: 0;
																			}
																		}

																		@keyframes sk-scaleout {
																			0% { 
																				-webkit-transform: scale(0);
																				transform: scale(0);
																				} 100% {
																					-webkit-transform: scale(1.0);
																					transform: scale(1.0);
																					opacity: 0;
																				}
																			}

																			#clickInci {
																				display: none;
																				position: absolute;
																				top: 350px;
																				left: calc(50% + 130px);
																				z-index: 99999999;
																				pointer-events: none;
																			}
																			#tour {
																				padding: 20px;
																				background-color: #fff;
																				font-family: "Poppins";
																				width: 300px;
																				border: 1px solid #ccc;
																				box-shadow: 0px 4px 20px rgba(0,0,0,.15);
																				border-radius: 6px;
																			}

																			#tour a {
																				float: right;
																				color: white;
																				padding: 8px 24px;
																				max-height: 32px;
																				background: #006179;
																				border-radius: 6px;
																			}

																			#tour h1 {
																				font-size: 20px;
																				padding: 0;
																				margin: 0;
																			}

																			#tour p {
																				padding: 0;
																				margin:6px 0px;
																				font-size: 14px;
																			}

																			#tour a {
																				font-size: 14px;
																			}
																			#dot {
																				position: absolute;
																				left: -30px;
																				top: 50%;
																				transform: translateY(-50%);
																				margin: 0 auto;
																			}

																			#dot,#dot1 {
																				height: 16px;
																				width: 16px;
																				background: #fff;
																				border-radius: 100%;
																				transition: opacity .3s, transform .3s ease;
																			}

																			#dot1, #dot2{
																				position: relative;
																				top: 0;
																				display: inline-block;
																				opacity: 0;
																			}

																			#dot1
																			{
																				display: block;

																				animation: fadeInFromNone 1.25s ease-out;
																				animation-iteration-count:infinite;
																			}

																			@-webkit-keyframes fadeInFromNone {
																				0% {
																					transform: scale3d(1,1,1);
																					display: none;
																					opacity: 0;
																				}

																				5% {
																					display: block;
																					opacity: .5;
																				}

																				100% {
																					transform: scale3d(4,4,1); 
																					display: block;
																					opacity: 0;
																				}
																			}

																			@-moz-keyframes fadeInFromNone {
																				0% {
																					display: none;
																					opacity: 0;
																				}

																				1% {
																					display: block;
																					opacity: 0;
																				}
																			}
																			#iframeStory {
																				display: none;
																				position: fixed;
																				top: 0;
																				bottom: 0;
																				left: 0;
																				right: 0;
																				background-color: #EFEFEF;
																				z-index: 9999;
																				min-height: 700px;
																			}

																			#iframeDevice {
																				position: absolute;
																				top: 100px;
																				left: 50%;
																				transform: translateX(-50%);
																				z-index: 9999999;
																			}
																			#iframeBack {
																				z-index: 9999999;
																				position: absolute;
																				top:40px;
																				right:40px;
																				display:flex;
																				align-items: center;
																				color:#999;

																			}

																			#iframeBack i{
																				font-size:35px;
																				margin-left:20px;
																			}

																			#iframeBack:hover{
																				color:#bbb;
																			}

																			#iframeBack:before{
																				display:none;
																			}

																			#menu-item-358 > .nav-link{
																				opacity: 0.8;
																			}

																			#menu-item-358 > .nav-link:hover{
																				opacity: 1;	
																			}

																			#menu-item-358 > .nav-link:before{
																				visibility: hidden;
																				-webkit-transform: scaleX(0);
																				-moz-transform: scaleX(0);
																				-ms-transform: scaleX(0);
																				-o-transform: scaleX(0);
																				transform: scaleX(0);
																			}

																			#menu-item-358 > .nav-link:hover:before{
																				visibility: visible;
																				-webkit-transform: scaleX(1);
																				-moz-transform: scaleX(1);
																				-ms-transform: scaleX(1);
																				-o-transform: scaleX(1);
																				transform: scaleX(1);
																			}

																		</style>
																		<script type="text/javascript">
																			(function() {

																			});

																			(function($) {


																				$(window).load(function() {
																					focus();
																					var listener = window.addEventListener('blur', function() {
																						if (document.activeElement === document.getElementById('iframe')) {
																							$('#clickInci').fadeOut(300);
																						}
																						window.removeEventListener('blur', listener);
																					});
																					var iframeLoaded = false;
																					$('#storyButton').click(function(e){
																						$('#iframeStory').show();

						// if (!iframeLoaded) {
						// 	var iframe = document.createElement("iframe");
						// 	iframe.style.display = "none";
						// 	iframe.width = "310";
						// 	iframe.height = "551";
						// 	iframe.id="stt-iframe"
						// 	iframe.onload = function (){
						// 		iframe.style.display = "block";
						// 		$('#clickInci').fadeIn(300);
						// 		$('#stt-iframe').contents().find('body').click(function(){
						// 			$('#clickInci').fadeOut(300);
						// 		});
						// 		iframeLoaded = true;
						// 	};
						// 	iframe.src = '{{url('/')}}';
						// 	document.getElementById("iframeDevice").appendChild(iframe);
						// }
					});

																					setTimeout(function(){
																						$('#storyButton').addClass('animate');
																					},1000);

																					$('#iframeBack').click(function(e){
																						e.preventDefault();
																						$('#iframeStory').hide();
																						document.getElementById("iframeDevice").removeChild(iframe);
																					});

																					$('#storyButton').hover(function(){
																						$('#storyBlock').addClass('open');
																						$(this).addClass('open');
																					}, function() {
																						$('#storyBlock').removeClass('open');
																					});

																				});

																			})( jQuery );
																		</script>
																		<body class="home page-template-default page page-id-328 wp-custom-logo home-personal clearfix wpb-js-composer js-comp-ver-5.4.5 vc_responsive" id="the-body">
																			<div id="iframeStory">
																				<div id="clickInci">
																					<div id="dot">
																						<div id="dot1">

																						</div>
																					</div>
																					<div id="tour">
																						<h1>Click to browse story !</h1>
																						<p>Snapchat and Instagram stories display like</p>
																					</div>
																				</div>
																				<div class="loader">
																					<div class="spinner">

																					</div>
																				</div>
																				<div class="phone-cont">
																					<div class="phone">
																						<div class="phone-shadow">

																						</div>
																						<div class="phone-top">

																						</div>
																						<div class="phone-bottom">

																						</div>
																						<div class="screen">

																						</div>
																						<div class="home">

																						</div>
																						<div class="speaker">

																						</div>
																					</div>
																				</div>
																				<div id="iframeDevice">

																				</div>
																				<a href="#" id="iframeBack" class="">
																					<div>Back to desktop mode</div><i class="fa fa-times">

																					</i></a>
																				</div>

																				<!-- Story button -->
																				<div id="storyButton">
																					<div id="chat-first-border" class="rotating">

																					</div>
																					<div id="chat-second-border">

																					</div>
																					<div id="chat-content"><i>1</i><img alt="Story demo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAI4klEQVR42u3cMYgdxx3H8f/M7O5dIQJR0thcEYIjUC9SqROOHRScuL2UUSuCsIlx4+A0wsTBGLdW6WttHEJMjDpVRr2IMSGFiJugwqjQm92ZSaETEUK2rNO7m/f7v++nlpb/3s33Zt++fS/YFiulTLXW82Z2obV2Lsb4QmttL6U0hRB6j4eHtNaslJJDCLdrrV+FEG6a2fUY442UUu49Xy9buUrneT5jZpdDCPsppdO958HRlVLutNYOzOyDcRy/7D3PSduqgJdl2au1Xk0p7ccYY+95sD611lpKOYgxvjkMw+3e85yUrQk453wppfRejPFU71lwfGqtd0spV6Zp+rD3LCfBfcDLsuyWUq5N07TfexacnJzzQUrpd8Mw3Os9y3FyHXAp5Qe11r8Nw3C+9yw4efM830gpXUwpfdN7luPiNuBlWXZrrZ+P40i8W+ww4hdTSi53Yrc3cmqt14gX4zieX5blWu85jovLgHPOl8Zx5DUvzMxsmqb9nPOl3nMcB3eX0Muy7IUQbnG3GQ+rtd5trZ319haTux241nqVePGoGOOpWuvV3nOsm6sdeJ7nMzHGWzykgcep95319MSWt4V+mXjxbQ7XxuXec6yTmx24lDK11r7m2WZ8l1LKnRDCc14+AOFmt6q1nidePElK6fThJ9BccBOwmV3oPQBkuFkrngI+13sAyHCzVtwEHEJ4ofcM0OBprbgJuLW213sGaPC0Vtzcha61Nr4GB99Ha81ijC4Wi5sd2Fu8OefPlmV5aVmWH4ZO5nn+0bIsL+WcP+v981gnT2vFzZm01lrvGdZltVr9cXd390+953jYvXv33trZ2Xm79xzrEpxU7OIkzPwEnHP+bGdn55e953ic1Wr192maXu49xzp4CdjNJbQXMcb3es+gONu2IuDN80XvAURn20oEDAgj4M2zyU8JbfJsW4mAN0yt9bXeMyjOtq0IeMNM0/TyarV6q/ccj1qtVm97uQPtiYtb6WZ+3kZ6YJ7nf4QQ/tJau1lrvdtjhhjjqRDCudbaa+M4/qL3z2SdvLyN5OIkzPwFjOPlJWAuoQFhBAwII2BAGAEDwggYEEbAgDACBoQRMCCMgAFhBAwII2BAGAEDwggYEEbAgDACBoQRMCCMgAFhBAwIG3oPsKmWZbnZewb83zAMfKXtY7j4XiCz9X8nlpfvTPKC3+/jcQkNCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwAgaEETAgjIABYQQMCCNgQBgBA8KG3gNsqtZa6z0D8CTswIAwAgaEETAgjIABYQQMCCNgQBgBA8IIGBBGwIAwnsRypNa6lFI+CSF8HEK4WWv9j5lZjPH51tq51tqrKaXfxBj5vTsReg+wLtv+6GPO+dMY4+/Hcfz3d/27eZ5/0lp7fxzHV3rP3FMIwcXad3ESZtsbcGutzvP8xs7OzrtP8/9yzq8Pw/BnJ+v4qXkJmNfA4o4Sr5nZNE3vLsvyRu/58Wxc/BUy284deJ7nT6dp+vWzHCPn/NdxHH/V+1xOmpcd2MVJmG1fwLXWpdb6sye95n2SeZ5/GmP857bd2PISMJfQomqtnzxrvGZm4zj+q9b6ae/zwdEQsK6PN/RYOEEELCqEcHONh/ui9/ngaAhY1IOHNNahtfbf3ueDoyFgQBgBi4oxPr+uY4UQftz7fHA0BCyqtXZujcf6ee/zwdEQsK5X13WgEMLajoWT5eLNbDMe5DgqHuTQxg4sKsY4tNbef9bjtNbe37Z4PSFgYeM4vrJarV4/6v/POf9hG5+D9oSAxY3j+M5RIj78OOE7vefHs3HxOsBs+14DP4oP9D8dL6+BXZyEGQGb8ZU6T4OAN0yttTn5neCYtdYsxuhisbh5DVxKyb1ngAZPa8VNwGZ2u/cAkOFmrXgK+KveA0CGm7XiKeB1fj4WvrlZK54Cvt57AMhws1Zc3IkzMyulTK21r1NKp3vPgs1VSrkTQngupeTiRpabHTillFtrB73nwGZrrR14idfM0Q5sZjbP85kY460Yo5s/TFifet/ZcRy/7D3Lurha6OM4fllKYRfGY5VSDjzFa+ZsBzYzW5ZlL4RwK8Z4qvcs2By11ruttbPDMLh5D9jM2Q5sZjYMw+1SypXec2CzlFKueIvXzOEO/MBqtfpomqb93nOgv5zzwc7Ozm97z3Ec3Aa8LMturfXzcRzP954F/SzLciPG+GJK6V7vWY6Du0voB4ZhuJdSurgsy43es6CPw3gveo3XzHHAZmYppW9ijC/mnLkzvWVyzgeHO+83vWc5Tm4voR+Vc76UUnqPu9O+1VrvllKuTNP0Ye9ZTsLWBGx2/y2mWuvVlNI+D3v4UmutpZSDGOObHu82f5utCviBeZ7PmNnlEMI+z05rK6XcOXyE9gNvD2l8H1sZ8AOllKnWet7MLpjZuRDCC621vZTSxNfzbJbWmpVScgjhdmvtK7v/kcDrMcYbnp5tflr/AwjCoTBcvdMsAAAAAElFTkSuQmCC">

																					</div>
																				</div>
																				<div id="storyBlock">
																					<div class="title">Story demo here !</div>
																					<div class="text">Wanna see stories display ?</div>
																				</div>
																				<div class="overflow-hidden">
																					<nav class="navbar navbar-expand-lg navbar-white fixed-top navbar-simple p-0 ">
																						<div class="divider navbar-skew-svg-darken">
																							<svg version='1.1' x='0px' y='0px' viewBox='0 0 240 24' enable-background='new 0 0 240 24' xml:space='preserve' preserveAspectRatio='none'>
																								<line x1="0" y1="100%" x2="100%" y2="0" />
																							</svg>
																						</div>
																						<div class="divider navbar-skew-svg">
																							<svg version='1.1' x='0px' y='0px' viewBox='0 0 240 24' enable-background='new 0 0 240 24' xml:space='preserve' preserveAspectRatio='none'>
																								<path d='M0,0v24L240,0H0z'>

																								</path>
																							</svg>
																						</div>
																						<div class="container-fluid">
																							<a class="navbar-brand" href="{{url('/')}}">
																								<img class="logo" src="static/images/logo-starstories.png" alt="Stories theme">						</a>

																								<button class="navbar-toggler rounded-circle" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
																									<span class="navbar-toggler-icon">
																										<span class="icon-one">
																											<span class="hamburger hamburger-one"></span>
																										</span>
																									</span>
																								</button>

																								<div class="align-self-start collapse navbar-collapse" id="navbarsExampleDefault">
																									<div class="bg-primary-menu">

																									</div>
																									<div class="nav-container load">
																										<div class="rotate-big-title nav-title big-title-left">
																											<div class="text">
																											Menu...									</div>
																										</div>
																										<div class="nav-navs d-flex flex-lg-row flex-column order-2 justify-content-center justify-content-lg-end">
																											<ul class="navbar-icons text-center p-0 order-1 order-lg-2">
																												<li class="d-block d-lg-none mx-2">
																													<!-- search -->
																													<form class="search" method="get" action="https://stories.star-themes.com/wellness" role="search">
																														<button class="search-submit" type="submit"><i class="fa fa-search" aria-hidden="true">

																														</i><span class="text d-none">Search</span></button>
																														<input class="search-input" type="search" name="s" placeholder="Search...">
																													</form>
																													<!-- /search -->										</li>
																													<li class="d-none d-lg-inline-block">
																														<div tabindex="0" class="search-submit trigger"><i class="fa fa-search" aria-hidden="true">

																														</i><span class="text d-none">Search</span>

																													</div>
																												</li>
																											</ul>
																											<div class="sg-headermenucont order-2 order-lg-1">
																												<ul id="menu-menu-1" class="navbar-nav navbar-nav-principal">
																													<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1551" class="visible-xs menu-item menu-item-type-post_type menu-item-object-page menu-item-1551 nav-item">
																														<a title="Stories" href="{{url('/')}}" class="nav-link">Stories</a>
																													</li>
																													<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-358" class="stt-menuitem-noactive menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-358 nav-item dropdown active">
																														<a title="Home" href="{{url('/')}}" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link" id="menu-item-dropdown-358">Home</a>
																														
																													</li>
																													<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-146" class="megamenu menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children menu-item-146 nav-item dropdown">
																														<a title="Categories" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link" id="menu-item-dropdown-146">Categories<span class="stt-dropdown-icon fa fa-plus"></span></a>
																														<div role="menu" class="dropdown-menu" aria-labelledby="menu-item-dropdown-146">
																															<div class='dropdown-cont container-fluid'>
																																<div class='bg-mega'>

																																</div>
																																<div class='divider divider-top'>
																																	<svg version='1.1' x='0px' y='0px' viewBox='0 0 240 24' enable-background='new 0 0 240 24' xml:space='preserve' preserveAspectRatio='none'>
																																		<path d='M240,24V0L0,24H240z'>

																																		</path>
																																	</svg>

																																</div>
																																<div class='divider divider-bottom'>
																																	<svg version='1.1' x='0px' y='0px' viewBox='0 0 240 24' enable-background='new 0 0 240 24' xml:space='preserve' preserveAspectRatio='none'>
																																		<path d='M0,0v24L240,0H0z'>

																																		</path>
																																	</svg>

																																</div>
																																<div class='navtitle'>Menu...</div>
																																<div>
																																	{{-- <ul class='row'>
																																		<li class="col">
																																			<div role="menu" class=" dropdown-submenu" >
																																				<ul>		
																																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-362" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-362 nav-item dropdown active">
																																						<a title="Homes" href="home-standard/index.html" class="dropdown-item dropdown-toggle">Homes<span class="stt-dropdown-icon fa fa-plus"></span></a>
																																						<div role="menu" class=" dropdown-menu" >
																																							<ul>			
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-363" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-363 nav-item dropdown-submenu">
																																									<a title="Home standard" href="home-standard/index.html" class="dropdown-item">Home standard</a>
																																								</li>
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-360 nav-item dropdown-submenu">
																																									<a title="Home Instagrammers" href="home-instagrammers/index.html" class="dropdown-item">Home Instagrammers</a>
																																								</li>
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-361" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-328 current_page_item menu-item-361 nav-item dropdown-submenu active">
																																									<a title="Home personal" href="index.html" class="dropdown-item">Home personal</a>
																																								</li>
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-918" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-918 nav-item dropdown-submenu">
																																									<a title="Home magazine" href="home-magazine/index.html" class="dropdown-item">Home magazine</a>
																																								</li>
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-814" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-814 nav-item dropdown-submenu">
																																									<a title="Home brand" href="home-brand/index.html" class="dropdown-item">Home brand</a>
																																								</li>
																																							</ul>

																																						</div>
																																					</li>
																																				</ul>

																																			</div>
																																		</li>
																																		<li class="col">
																																			<div role="menu" class=" dropdown-submenu" >
																																				<ul>		
																																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-144" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-144 nav-item dropdown">
																																						<a title="About us" href="about-us-01/index.html" class="dropdown-item dropdown-toggle">About us<span class="stt-dropdown-icon fa fa-plus"></span></a>
																																						<div role="menu" class=" dropdown-menu" >
																																							<ul>			
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-161" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-161 nav-item dropdown-submenu">
																																									<a title="About us 01" href="about-us-01/index.html" class="dropdown-item">About us 01</a>
																																								</li>
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-160" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-160 nav-item dropdown-submenu">
																																									<a title="About us 02" href="about-us-02/index.html" class="dropdown-item">About us 02</a>
																																								</li>
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-189" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-189 nav-item dropdown-submenu">
																																									<a title="About us 03" href="about-us-03/index.html" class="dropdown-item">About us 03</a>
																																								</li>
																																							</ul>

																																						</div>
																																					</li>
																																				</ul>

																																			</div>
																																		</li>
																																		<li class="col">
																																			<div role="menu" class=" dropdown-submenu" >
																																				<ul>		
																																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-157" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-157 nav-item dropdown">
																																						<a title="Services" href="#" class="dropdown-item dropdown-toggle">Services<span class="stt-dropdown-icon fa fa-plus"></span></a>
																																						<div role="menu" class=" dropdown-menu" >
																																							<ul>			
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-162 nav-item dropdown-submenu">
																																									<a title="Services 01" href="services-01/index.html" class="dropdown-item">Services 01</a>
																																								</li>
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220 nav-item dropdown-submenu">
																																									<a title="Services 02" href="services-02/index.html" class="dropdown-item">Services 02</a>
																																								</li>
																																								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-221" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221 nav-item dropdown-submenu">
																																									<a title="Services 03" href="services-03/index.html" class="dropdown-item">Services 03</a>
																																								</li>
																																							</ul>

																																						</div>
																																					</li>
																																				</ul>

																																			</div>
																																		</li>
																																		<li class="col">
																																			<div role="menu" class=" dropdown-submenu" >
																																				<ul>		
																																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1667" class="menu-item menu-item-type-post_type menu-item-object-megamenumedia menu-item-1667 nav-item">
																																						<a title="menu-1" href="#" class="dropdown-item"><img src="/static/images/im-cta-small.png" alt=""></a>
																																					</li>
																																				</ul>

																																			</div>
																																		</li>
																																	</ul> --}}
																																</div>
																															</div>
																														</div>
																													</li>
																													<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-776" class="megamenu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-776 nav-item dropdown">
																														<a title="Blog" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link" id="menu-item-dropdown-776">Blog<span class="stt-dropdown-icon fa fa-plus"></span></a>
																														<div role="menu" class="dropdown-menu" aria-labelledby="menu-item-dropdown-776">
																															<div class='dropdown-cont container-fluid'>
																																<div class='bg-mega'>
																																</div>
																																<div class='divider divider-top'>
																																	<svg version='1.1' x='0px' y='0px' viewBox='0 0 240 24' enable-background='new 0 0 240 24' xml:space='preserve' preserveAspectRatio='none'>
																																		<path d='M240,24V0L0,24H240z'>

																																		</path>
																																	</svg>
																																</div>
																																<div class='divider divider-bottom'>
																																	<svg version='1.1' x='0px' y='0px' viewBox='0 0 240 24' enable-background='new 0 0 240 24' xml:space='preserve' preserveAspectRatio='none'>
																																		<path d='M0,0v24L240,0H0z'>

																																		</path>
																																	</svg>
																																</div>
																																<div class='navtitle'>Menu...</div>
																																<div><ul class='row'>
																																	<li class="col">
																																		<div role="menu" class=" dropdown-submenu" >
																																			<ul>		<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-791" class="menu-item menu-item-type-post_type menu-item-object-megamenumedia menu-item-791 nav-item"><img src="/static/images/icon-menu-1.png" alt="">
																																			</li>
																																			<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-780" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-780 nav-item dropdown">
																																				<a title="Blog pages" href="#" class="dropdown-item dropdown-toggle">Blog pages<span class="stt-dropdown-icon fa fa-plus"></span></a>
																																				<div role="menu" class=" dropdown-menu" >
																																					<ul>			<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9 nav-item dropdown-submenu">
																																						<a title="Stories (Archive page)" href="{{url('/')}}" class="dropdown-item">Stories (Archive page)</a>
																																					</li>
																																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1603" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1603 nav-item dropdown-submenu">
																																						<a title="Split article" href="split-news-1/index.html" class="dropdown-item">Split article</a>
																																					</li>
																																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1604" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1604 nav-item dropdown-submenu">
																																						<a title="Split no sidebar" href="split-news-4/index.html" class="dropdown-item">Split no sidebar</a>
																																					</li>
																																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1601" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1601 nav-item dropdown-submenu">
																																						<a title="Simple article" href="simple-news-1/index.html" class="dropdown-item">Simple article</a>
																																					</li>
																																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1602" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1602 nav-item dropdown-submenu">
																																						<a title="Simple no sidebar" href="simple-news-4/index.html" class="dropdown-item">Simple no sidebar</a>
																																					</li>
																																				</ul>
																																			</div>
																																		</li>
																																	</ul>
																																</div>
																															</li>
																															<li class="col">
																																<div role="menu" class=" dropdown-submenu" >
																																	<ul>		<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-794" class="menu-item menu-item-type-post_type menu-item-object-megamenumedia menu-item-794 nav-item"><img src="/static/images/icon-menu-2.png" alt="">
																																	</li>
																																	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-801" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-801 nav-item dropdown">
																																		<a title="Post type" href="#" class="dropdown-item dropdown-toggle">Post type<span class="stt-dropdown-icon fa fa-plus"></span></a>
																																		<div role="menu" class=" dropdown-menu" >
																																			<ul>			<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1548" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1548 nav-item dropdown-submenu">
																																				<a title="Standard post" href="simple-news-1/index.html" class="dropdown-item">Standard post</a>
																																			</li>
																																			<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1549" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1549 nav-item dropdown-submenu">
																																				<a title="Image (social feed)" href="social-feed-post-2/index.html" class="dropdown-item">Image (social feed)</a>
																																			</li>
																																			<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1600" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1600 nav-item dropdown-submenu">
																																				<a title="video (social feed)" href="social-media-video/index.html" class="dropdown-item">video (social feed)</a>
																																			</li>
																																			<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1542" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1542 nav-item dropdown-submenu">
																																				<a title="Video post" href="yoga-for-beginners/index.html" class="dropdown-item">Video post</a>
																																			</li>
																																			<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1547" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1547 nav-item dropdown-submenu">
																																				<a title="Quote post" href="quote-1/index.html" class="dropdown-item">Quote post</a>
																																			</li>
																																		</ul>
																																	</div>
																																</li>
																															</ul>
																														</div>
																													</li>
																													<li class="col">
																														<div role="menu" class=" dropdown-submenu" >
																															<ul>		<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-811" class="menu-item menu-item-type-post_type menu-item-object-megamenumedia menu-item-811 nav-item"><img src="/static/images/icon-menu-2.png" alt="">
																															</li>
																															<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-779" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-779 nav-item dropdown">
																																<a title="Categories" href="#" class="dropdown-item dropdown-toggle">Categories<span class="stt-dropdown-icon fa fa-plus"></span></a>
																																<div role="menu" class=" dropdown-menu" >
																																	<ul>			<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1026" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1026 nav-item dropdown-submenu">
																																		<a title="Social feed" href="category/social-feed/index.html" class="dropdown-item">Social feed</a>
																																	</li>
																																	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-227" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-227 nav-item dropdown-submenu">
																																		<a title="New moves" href="category/new-moves/index.html" class="dropdown-item">New moves</a>
																																	</li>
																																	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-1025" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1025 nav-item dropdown-submenu">
																																		<a title="Stories" href="category/stories/index.html" class="dropdown-item">Stories</a>
																																	</li>
																																	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-228" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-228 nav-item dropdown-submenu">
																																		<a title="News" href="category/news/index.html" class="dropdown-item">News</a>
																																	</li>
																																	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-230" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-230 nav-item dropdown-submenu">
																																		<a title="Best quote" href="category/best-quote/index.html" class="dropdown-item">Best quote</a>
																																	</li>
																																</ul>
																															</div>
																														</li>
																													</ul>
																												</div>
																											</li>
																											<li class="col">
																												<div role="menu" class=" dropdown-submenu" >
																													<ul>		<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-787" class="media-up menu-item menu-item-type-post_type menu-item-object-megamenumedia menu-item-787 nav-item"><img src="/static/images/im-cta-menu.png" alt="">
																													</li>
																												</ul>
																											</div>
																										</li>
																									</ul>
																								</div>
																							</div>

																						</div>
																					</li>
																					<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-773" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-773 nav-item dropdown">
																						<a title="Features" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link" id="menu-item-dropdown-773">Features<span class="stt-dropdown-icon fa fa-plus"></span></a>
																						<div role="menu" class="dropdown-menu" aria-labelledby="menu-item-dropdown-773">
																							<div class='dropdown-cont container-fluid'>
																								<div class='bg-mega'>

																								</div>
																								<div class='divider divider-top'>
																									<svg version='1.1' x='0px' y='0px' viewBox='0 0 240 24' enable-background='new 0 0 240 24' xml:space='preserve' preserveAspectRatio='none'>
																										<path d='M240,24V0L0,24H240z'>

																										</path>
																									</svg>

																								</div>
																								<div class='divider divider-bottom'>
																									<svg version='1.1' x='0px' y='0px' viewBox='0 0 240 24' enable-background='new 0 0 240 24' xml:space='preserve' preserveAspectRatio='none'>
																										<path d='M0,0v24L240,0H0z'>

																										</path>
																									</svg>

																								</div>
																								<div class='navtitle'>Menu...</div>
																								<div><ul class='row'>
																									<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-245" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-245 nav-item">
																										<a title="Text features" href="text-features/index.html" class="dropdown-item">Text features</a>
																									</li>
																									<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-904" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-904 nav-item">
																										<a title="Image features" href="image-features/index.html" class="dropdown-item">Image features</a>
																									</li>
																									<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-903" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-903 nav-item">
																										<a title="Blog features" href="blog-features/index.html" class="dropdown-item">Blog features</a>
																									</li>
																								</ul>
																							</div>
																						</div>

																					</div>
																				</li>
																				<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-293" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-293 nav-item">
																					<a title="Contact" href="contact/index.html" class="nav-link">Contact</a>
																				</li>
																			</ul>										<div class="mega-arrow-cont position-absolute d-none d-lg-block">
																				<div class="mega-arrow">

																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="search-head d-none d-lg-block">
															<!-- search -->
															<form class="search" method="get" action="https://stories.star-themes.com/wellness" role="search">
																<button class="search-submit" type="submit"><i class="fa fa-search" aria-hidden="true">

																</i><span class="text d-none">Search</span></button>
																<input class="search-input" type="search" name="s" placeholder="Search...">
															</form>
															<!-- /search -->					</div>
														</nav>	<main class="p100 title-hidden stt-main-content">

															<h1 class="d-none">Home personal</h1>
															<!-- article -->


															@yield('content')



															<footer class="website-footer footer-dark">
																<div class="container-fluid">
																	<div class="row logo-social justify-content-end">
																		<div class="col-auto">
																			<div class="social-footer"><ul class="social-nav">
																				<li class="d-inline-block">
																					<a href="#"><i class="fa fa-facebook" aria-hidden="true">

																					</i></a>
																				</li>
																				<li class="d-inline-block">
																					<a href="#"><i class="fa fa-twitter" aria-hidden="true">

																					</i></a>
																				</li>
																				<li class="d-inline-block">
																					<a href="#"><i class="fa fa-pinterest" aria-hidden="true">

																					</i></a>
																				</li></ul>

																			</div>							<a href="#" id="backToTop"><i class="ion-android-arrow-up">

																			</i></a>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-12 col-lg-6 info-brand footer-widgets-col">
																			<div id="text-2" class="widget widget_text">			<div class="textwidget"><p><img class="alignnone wp-image-1714" src="static/images/logo-footer-starstories.png" alt="" width="220" height="56"/></p>
																				<p>Consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa</p>
																			</div>
																		</div>
																		<div id="starstories_contact_block_widget-2" class="widget widget_starstories_contact_block_widget">
																			<div class="block-contact">
																				<div class="block-contact-col contact-link contact-link-address"><i class="im im-location">

																				</i>
																				<div class="contact-info"><h4 class="title-mt-0">Yoga blog</h4>115 design avenue<br />
																				New york 15</div>
																			</div>
																			<div class="block-contact-col block-contact-col-paddings">
																				<div class="contact-link"><i class="im im-phone">

																				</i>
																				<div class="contact-info">
																					<a href="tel:555 568 987">555 568 987</a>
																				</div>
																			</div>
																			<div class="contact-link"><i class="im im-mail">

																			</i>
																			<div class="contact-info">
																				<a href="mailto:contact@mysite.com">contact@mysite.com</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>						
														</div><!-- end col info-brand -->
														<div class="px-sm-5 col">
															<div class="row">
																<div class="col-12 col-sm footer-widgets-col">
																	<div id="nav_menu-2" class="widget widget_nav_menu"><h4 class="widget-title title-mt-0 h5">Quick access</h4>
																		<div class="menu-quick-access-1-container"><ul id="menu-quick-access-1" class="menu">
																			<li id="menu-item-1309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1309">
																				<a href="home-standard/index.html">Home standard</a>
																			</li>
																			<li id="menu-item-1306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1306">
																				<a href="home-instagrammers/index.html">Home Instagrammers</a>
																			</li>
																			<li id="menu-item-1308" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-328 current_page_item menu-item-1308">
																				<a href="index.html">Home personal</a>
																			</li>
																			<li id="menu-item-1307" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1307">
																				<a href="home-magazine/index.html">Home magazine</a>
																			</li>
																			<li id="menu-item-1305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1305">
																				<a href="home-brand/index.html">Home brand</a>
																			</li>
																		</ul>
																	</div>

																</div>								</div>
																<div class="col-12 col-sm footer-widgets-col">
																	<div id="nav_menu-3" class="widget widget_nav_menu"><h4 class="widget-title title-mt-0 h5">Blog categories</h4>
																		<div class="menu-quick-access-2-container"><ul id="menu-quick-access-2" class="menu">
																			<li id="menu-item-1311" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1311">
																				<a href="category/stories/index.html">Stories</a>
																			</li>
																			<li id="menu-item-1310" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1310">
																				<a href="category/new-moves/index.html">New moves</a>
																			</li>
																			<li id="menu-item-1312" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1312">
																				<a href="category/news/index.html">News</a>
																			</li>
																			<li id="menu-item-1313" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1313">
																				<a href="category/social-feed/index.html">Social feed</a>
																			</li>
																			<li id="menu-item-1314" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1314">
																				<a href="category/advertising/index.html">Advertising</a>
																			</li>
																			<li id="menu-item-1315" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1315">
																				<a href="category/best-quote/index.html">Best quote</a>
																			</li>
																		</ul>
																	</div>

																</div>								</div>
															</div><!-- end row -->
														</div>
													</div> <!-- end row -->
													<hr>
													<div class="row footer-bottom">
														<div class="col">
															<div id="text-3" class="widget widget_text ">			<div class="textwidget">
																<div class="text-center">Star Stories theme © 2019 | <a href="#">Privacy Notice</a> I <a href="#">Terms &amp; Conditions</a> I <a href="#">Site map</a> I <a href="mailto:contact@stories-theme.com">Contact</a>

																</div>
															</div>
														</div>						</div>
													</div>
												</div>
											</footer>
										</div> <!-- End overflow hidden -->
										<link rel='stylesheet' id='animate-css-css'  href='static/css/animate.min5243.css?ver=5.4.5' type='text/css' media='all' />
										<script type='text/javascript' src='/static/js/conditionizr-4.3.0.minb2f9.js?ver=4.3.0'></script>
										<script type='text/javascript' src='/static/js/popper.minc8f9.js?ver=4.9.13'></script>
										<script type='text/javascript' src='/static/js/bootstrap.mincce7.js?ver=4.0.0'></script>
										<script type='text/javascript' src='/static/js/gmap3.minaf8e.js?ver=6.0.0'></script>
										<script type='text/javascript' src='/static/js/slick/slick.minc8f9.js?ver=4.9.13'></script>
										<script type='text/javascript' src='/static/js/hammer/hammer.mina7f4.js?ver=2.0.8'></script>
										<script type='text/javascript' src='/static/js/jquery.waitforimages.minc8f9.js?ver=4.9.13'></script>
										<script type='text/javascript' src='/static/js/imagesloaded.min55a0.js?ver=3.2.0'></script>
										<script type='text/javascript' src='/static/js/starstories-js.min8a54.js?ver=1.0.0'></script>
										<script type='text/javascript'>
											(function($) {
												$(document).ready(function () {
													"use strict";
													$("#map_5e5a6c6a2355f").bind('gmap-reload', function() {
														starstories_gmap3_init();
													});

													starstories_gmap3_init();

													function starstories_gmap3_init() {
														$("#map_5e5a6c6a2355f").gmap3('destroy');

														$("#map_5e5a6c6a2355f").gmap3({
															marker: {
																values: [{address: " New York 15 Ford Street Brooklyn" , data:"New York 15 Ford Street Brooklyn", options:{icon: "/static/images/map-marker.png"}}],
																events:{
																	click: function(marker, event, context){
																		var map = jQuery(this).gmap3("get"),
																		infowindow = $(this).gmap3({get:{name:"infowindow"}});
																		if (infowindow){
																			infowindow.open(map, marker);
																			infowindow.setContent('<div class="noscroll">'+context.data+'</div>');
																		} else {
																			$(this).gmap3({
																				infowindow:{
																					anchor:marker,
																					options:{content: '<div class="noscroll">'+context.data+'</div>'}
																				}
																			});
																		}
																	}
																}
															},
															map: {
																options: {
																	zoom: 14,
																	zoomControl: true,
																	zoomControlOptions: {
																		position: google.maps.ControlPosition.RIGHT_TOP
																	},styles:[{'featureType':'landscape','stylers':[{'saturation':-100},{'lightness':65},{'visibility':'on'}]},{'featureType':'poi','stylers':[{'saturation':-100},{'lightness':51},{'visibility':'simplified'}]},{'featureType':'road.highway','stylers':[{'saturation':-100},{'visibility':'simplified'}]},{'featureType':'road.arterial','stylers':[{'saturation':-100},{'lightness':30},{'visibility':'on'}]},{'featureType':'road.local','stylers':[{'saturation':-100},{'lightness':40},{'visibility':'on'}]},{'featureType':'transit','stylers':[{'saturation':-100},{'visibility':'simplified'}]},{'featureType':'administrative.province','stylers':[{'visibility':'off'}]},{'featureType':'water','elementType':'labels','stylers':[{'visibility':'on'},{'lightness':-25},{'saturation':-100}]},{'featureType':'water','elementType':'geometry','stylers':[{'hue':'#ffff00'},{'lightness':-25},{'saturation':-97}]}],scrollwheel: false,
																	streetViewControl: false,
																	mapTypeControl: false
																}
															}
														});
													}
												});$("head").append("<style>#map_5e5a6c6a2355f {height: 550px;}</style>");})(jQuery);
											</script>
											<script type='text/javascript'>
												/* <![CDATA[ */
												var wpcf7 = {"apiSettings":{"root":"https:\/\/stories.star-themes.com\/wellness\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
												/* ]]> */
											</script>
											<script type='text/javascript' src='static/js/scriptsaead.js?ver=5.0.3'></script>
											<script type='text/javascript' src='static/js/masonry.mind617.js?ver=3.3.2'></script>
											<script type='text/javascript' src='/static/js/wp-embed.minc8f9.js?ver=4.9.13'></script>
											<script type='text/javascript' src='/static/js/js_composer_front.min5243.js?ver=5.4.5'></script>
											<script type='text/javascript' src='/static/js/waypoints.min5243.js?ver=5.4.5'></script>
											<script type='text/javascript' src='/static/js/skrollr.min5243.js?ver=5.4.5'></script>
											<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAtC3dy2DFWOQrYVBi-ajs_Bt7GPwir4to&amp;ver=4.9.13'></script>
										</body>
										</html>