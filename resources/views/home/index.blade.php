@extends('layouts.static')

@section('content')

<article id="post-328" class="post-328 page type-page status-publish hentry">

	<div class="  "><section class="vc_custom_section my-none   skew-slider-section fullw primary"><div class="container-fluid pt-none pb-none"><div class="row"><div class="col-sm-12"><div class="vc_column-inner  withskewelem "><div class="wpb_wrapper"><div class="carousel-cont size-m"><div id="carousel-5e5a6c6a16ec5" class="carousel" data-ride="carousel" data-slidenb="0"><div class="loader"><div class="spinner"></div></div><ol class="carousel-indicators"><li data-target="#carousel-5e5a6c6a16ec5" data-slide-to="0" class="active"></li><li data-target="#carousel-5e5a6c6a16ec5" data-slide-to="1"></li></ol><div class="carousel-item">
		<div class="bg-skew img-skew"><div class="cont-title v-pos-center justify-content-center"><div class="wrap-title"><div class="title">Story navigation<br />
		Video grid autoplay</div></div></div><div class="bg-cover bg-cover-pos-y-top bg-cover-pos-x-center" style="background-image:url(static/images/slide-2.jpg);">
		</div>
	</div>
</div><div class="carousel-item">
	<div class="bg-skew img-skew"><div class="cont-title v-pos-top justify-content-start"><div class="wrap-title"><div class="title">Home personal<br />
	Yoga blog</div></div></div><div class="bg-cover bg-cover-pos-y-top bg-cover-pos-x-center" style="background-image:url(static/images/slide-1.jpg);">
	</div>
</div>
</div><a class="carousel-control-prev" href="#carousel-5e5a6c6a16ec5" data-slide="prev">
	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
	<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carousel-5e5a6c6a16ec5" data-slide="next">
	<span class="carousel-control-next-icon" aria-hidden="true"></span>
	<span class="sr-only">Next</span>
</a></div></div></div></div></div></div></div></section></div><div class=" wpb_animate_when_almost_visible wpb_fadeIn fadeIn crop-wraper section-parent-wraper-rel "><div class="divider-section divider divider-bottom divider-light"><svg version="1.1" x="0px" y="0px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none">
	<path d="M240,24V0L0,24H240z"></path>
</svg></div><section class="vc_custom_section my-none   overflow-hidden skew-bottom"><div class="container-fluid pt-big pb-big"><div class="row"><div class="text-center col-sm-5 hidden-sm hidden-xs"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1553521287008">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="511" height="846" src="static/images/perso1.png" class="vc_single_image-img attachment-full" alt="" srcset="/static/images/perso1.png 511w, /static/images/perso1-250x414.png 250w, /static/images/perso1-120x199.png 120w, /static/images/perso1-325x538.png 325w, /static/images/perso1-100x165.png 100w, /static/images/perso1-199x330.png 199w, /static/images/perso1-173x286.png 173w, /static/images/perso1-345x572.png 345w, /static/images/perso1-111x183.png 111w, /static/images/perso1-221x366.png 221w, /static/images/perso1-211x349.png 211w, /static/images/perso1-422x698.png 422w, /static/images/perso1-181x300.png 181w" sizes="(max-width: 511px) 100vw, 511px" /></div>
		</figure>
	</div>
</div></div></div><div class="col-sm-7"><div class="vc_column-inner inner-in"><div class="wpb_wrapper"><h1 class=" title-mt-0 dark h1 line l-up line-primary text-left ">Hello Yoga fans</h1>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p>We have given our best to make an impressive theme with a really intuitive WordPress admin. You won&#8217;t find tons of parameters, just what you need to customize your website in a smart way. Like any social app, we have created a new way to navigate through your post especially on mobile. The Stories navigation is more user friendly &amp; more intuitive to seduce your reader.</p>

		</div>
	</div>
	<div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
	<div class="row inner"><div class="col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><ul class="icon-list primary-icons m-icons s-text "><li class=""><i class="im im-video-camera"></i><p>Lorem ipsum dolor</p>
	</li><li class=""><i class="im im-line-chart-up"></i><p>consectetur adipiscing</p>
	</li><li class=""><i class="im im-speech-bubble-comments"></i><p>Luctus nec ullamcorper</p>
	</li></ul></div></div></div><div class="col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><ul class="icon-list primary-icons m-icons s-text "><li class=""><i class="im im-video-camera"></i><p>Lorem ipsum dolor</p>
	</li><li class=""><i class="im im-line-chart-up"></i><p>consectetur adipiscing</p>
	</li><li class=""><i class="im im-speech-bubble-comments"></i><p>Luctus nec ullamcorper</p>
	</li></ul></div></div></div></div></div></div></div></div></div></section></div><div class="  "><section class="vc_custom_section my-none   light"><div class="rotate-big-title big-title-right wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp"><div class="text">Stories</div></div><div class="container-fluid pt-medium pb-medium"><div class="row"><div class="col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div 	class="vc_icon_element vc_icon_element-outer wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown vc_icon_element-align-center">
		<div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-xl vc_icon_element-style- vc_icon_element-background-color-grey">
			<span class="vc_icon_element-icon im im-fire" style="color:#c48ff5 !important"></span>	</div>
		</div>
		<div class="vc_empty_space"   style="height: 10px" ><span class="vc_empty_space_inner"></span></div>
		<h3 class=" title-mt-0 pb-0 dark h2 text-center mb-sub  wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown">Latest stories</h3><span class="subtitle h5 text-center  wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown">Like on Insta or Snap you can read illustrated stories with a simple touch.</span><div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
		<div class="cards-list  wpb_animate_when_almost_visible wpb_fadeIn fadeIn"><div class="cards-list-wrapper"><a class="position-relative card-link-cat " href="blog/index.html"><div class="card-link-cat-cont"><div class="bg-cover" style="background-image:url(static/images/social-feed-4-420x747.jpg)"></div><div class="card-icon"><i class="icon ion-social-instagram"></i></div><h4 class="text-white link-title title-mt-0">All Stories</h4><h5 class="cat-title title-mt-0">Blog</h5></div></a><a class="position-relative card-link-cat " href="#"><div class="card-link-cat-cont"><div class="bg-cover" style="background-image:url(static/images/news-4-vert-420x747.jpg)"></div><div class="card-icon"><i class="icon ion-ios-basketball"></i></div><h4 class="text-white link-title title-mt-0">Advanced poses</h4><h5 class="cat-title title-mt-0">Movements</h5></div></a><a class="position-relative card-link-cat " href="category/news/index.html"><div class="card-link-cat-cont"><div class="bg-cover" style="background-image:url(static/images/slide-2-420x747.jpg)"></div><div class="card-icon"><i class="icon ion-android-pin"></i></div><h4 class="text-white link-title title-mt-0">Women&#039;s health</h4><h5 class="cat-title title-mt-0">News</h5></div></a><a class="position-relative card-link-cat " href="category/best-quote/index.html"><div class="card-link-cat-cont"><div class="bg-cover" style="background-image:url(static/images/news-1-vert-420x747.jpg)"></div><div class="card-icon"><i class="icon ion-chatbubble-working"></i></div><h4 class="text-white link-title title-mt-0">Words of well-being</h4><h5 class="cat-title title-mt-0">Best quote</h5></div></a><a class="position-relative card-link-cat " href="#"><div class="card-link-cat-cont"><div class="bg-cover" style="background-image:url(static/images/image-3-420x600.jpg)"></div><div class="card-icon"><i class="icon ion-android-star"></i></div><h4 class="text-white link-title title-mt-0">Some tips</h4><h5 class="cat-title title-mt-0">And Tricks</h5></div></a><a class="position-relative card-link-cat " href="#"><div class="card-link-cat-cont"><div class="bg-cover" style="background-image:url(static/images/image-2-420x600.jpg)"></div><div class="card-icon"><i class="icon ion-social-dribbble-outline"></i></div><h4 class="text-white link-title title-mt-0">Exercises</h4><h5 class="cat-title title-mt-0">Breath</h5></div></a></div></div><div class="vc_empty_space"   style="height: 45px" ><span class="vc_empty_space_inner"></span></div>
		<div class="text-center"><a href="blog/index.html" class="btn btn-primary arrow  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp">More stories</a></div></div></div></div></div></div></section></div><div class=" crop-wraper section-parent-wraper-rel "><div class="divider-section divider divider-top divider-light"><svg version="1.1" x="0px" y="0px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none">
			<path d="M0,0v24L240,0H0z"></path>
		</svg></div><div class="divider-section divider divider-bottom divider-white"><svg version="1.1" x="0px" y="0px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none">
			<path d="M240,24V0L0,24H240z"></path>
		</svg></div><section class="vc_custom_section my-none   fullw overflow-hidden primary skew-top skew-bottom"><div class="container-fluid pt-none pb-none"><div class="row"><div class="wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft col-sm-6"><div class="vc_column-inner vc_custom_1552470132106"><div class="wpb_wrapper"><div class="img-cta-cont-container"><div class="img-cta-container size-cover ">
			<div class="bg-skew img-skew"><div class="bg-cover bg-cover-pos-y-center bg-cover-pos-x-center" style="background-image:url(static/images/image-1.jpg);">
			</div>
		</div>
	</div></div></div></div></div><div class="wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight col-sm-6"><div class="vc_column-inner vc_custom_1552657493355"><div class="wpb_wrapper"><h2 class=" title-mt-0 light h2 line l-left line-light text-left ">Zen lovers</h2>
		<div class="wpb_text_column wpb_content_element " >
			<div class="wpb_wrapper">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>

			</div>
		</div>
		<div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
		<div class="text-left"><a href="#" class="btn btn-light arrow">Read more</a></div></div></div></div></div></div></section></div><div class=" crop-wraper section-parent-wraper-rel "><div class="divider-section divider divider-top divider-white"><svg version="1.1" x="0px" y="0px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none">
			<path d="M0,0v24L240,0H0z"></path>
		</svg></div><div class="divider-section divider divider-bottom divider-white"><svg version="1.1" x="0px" y="0px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none">
			<path d="M240,24V0L0,24H240z"></path>
		</svg></div><section class="vc_custom_section my-none   fullw overflow-hidden dark skew-top skew-bottom"><div class="container-fluid pt-none pb-none"><div class="row"><div class="wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft col-sm-6"><div class="vc_column-inner vc_custom_1552657512948"><div class="wpb_wrapper"><h2 class=" title-mt-0 light h2 line l-left line-primary text-left ">Healthy life</h2>
			<div class="wpb_text_column wpb_content_element " >
				<div class="wpb_wrapper">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>

				</div>
			</div>
			<div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
			<div class="text-left"><a href="#" class="btn btn-light arrow">Read more</a></div></div></div></div><div class="wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight col-sm-6"><div class="vc_column-inner vc_custom_1551102936421"><div class="wpb_wrapper"><div class="img-cta-cont-container"><div class="img-cta-container size-cover ">
				<div class="bg-skew img-skew"><div class="bg-cover bg-cover-pos-y-center bg-cover-pos-x-center" style="background-image:url(static/images/slide-1.jpg);">
				</div>
			</div>
		</div></div></div></div></div></div></div></section></div><div class="  "><section class="vc_custom_section my-none  "><div class="rotate-big-title big-title-left wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp"><div class="text">Crew</div></div><div class="container-fluid pt-medium pb-big"><div class="row"><div class="col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div 	class="vc_icon_element vc_icon_element-outer wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown vc_icon_element-align-center">
			<div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-xl vc_icon_element-style- vc_icon_element-background-color-grey">
				<span class="vc_icon_element-icon icon ion-android-star" style="color:#c48ff5 !important"></span>	</div>
			</div>
			<div class="vc_empty_space"   style="height: 10px" ><span class="vc_empty_space_inner"></span></div>
			<h3 class=" title-mt-0 pb-0 dark h2 text-center mb-sub  wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown">The crew</h3><span class="subtitle h5 text-center  wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown">Lorem ipsum dolor sit amet eras deus ex machina consectum.</span><div class="vc_empty_space"   style="height: 25px" ><span class="vc_empty_space_inner"></span></div>
		</div></div></div><div class="wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp col-sm-3 col-6"><div class="vc_column-inner "><div class="wpb_wrapper">
			<div  class="wpb_single_image wpb_content_element vc_align_center">
				
				<figure class="wpb_wrapper vc_figure">
					<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="511" height="846" src="static/images/perso1.png" class="vc_single_image-img attachment-large" alt="" srcset="/static/images/perso1.png 511w, /static/images/perso1-250x414.png 250w, /static/images/perso1-120x199.png 120w, /static/images/perso1-325x538.png 325w, /static/images/perso1-100x165.png 100w, /static/images/perso1-199x330.png 199w, /static/images/perso1-173x286.png 173w, /static/images/perso1-345x572.png 345w, /static/images/perso1-111x183.png 111w, /static/images/perso1-221x366.png 221w, /static/images/perso1-211x349.png 211w, /static/images/perso1-422x698.png 422w, /static/images/perso1-181x300.png 181w" sizes="(max-width: 511px) 100vw, 511px" /></div><figcaption class="vc_figure-caption"><div class="bg-caption"></div><span class="caption-text">Yoga teacher</span></figcaption>
				</figure>
			</div>
			<div class="vc_empty_space"   style="height: 25px" ><span class="vc_empty_space_inner"></span></div>

			<div class="wpb_text_column wpb_content_element " >
				<div class="wpb_wrapper">
					<h5 style="text-align: center;"><span style="color: #c48ff5;">John Smith</span></h5>
					<p style="text-align: center;">I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

				</div>
			</div>
		</div></div></div><div class="wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp col-sm-3 col-6"><div class="vc_column-inner "><div class="wpb_wrapper">
			<div  class="wpb_single_image wpb_content_element vc_align_center">
				
				<figure class="wpb_wrapper vc_figure">
					<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="511" height="846" src="static/images/perso1.png" class="vc_single_image-img attachment-large" alt="" srcset="/static/images/perso1.png 511w, /static/images/perso1-250x414.png 250w, /static/images/perso1-120x199.png 120w, /static/images/perso1-325x538.png 325w, /static/images/perso1-100x165.png 100w, /static/images/perso1-199x330.png 199w, /static/images/perso1-173x286.png 173w, /static/images/perso1-345x572.png 345w, /static/images/perso1-111x183.png 111w, /static/images/perso1-221x366.png 221w, /static/images/perso1-211x349.png 211w, /static/images/perso1-422x698.png 422w, /static/images/perso1-181x300.png 181w" sizes="(max-width: 511px) 100vw, 511px" /></div><figcaption class="vc_figure-caption"><div class="bg-caption"></div><span class="caption-text">Yoga teacher</span></figcaption>
				</figure>
			</div>
			<div class="vc_empty_space"   style="height: 25px" ><span class="vc_empty_space_inner"></span></div>

			<div class="wpb_text_column wpb_content_element " >
				<div class="wpb_wrapper">
					<h5 style="text-align: center;"><span style="color: #c48ff5;">Jane Smith</span></h5>
					<p style="text-align: center;">I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

				</div>
			</div>
		</div></div></div><div class="wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp col-sm-3 col-6"><div class="vc_column-inner "><div class="wpb_wrapper">
			<div  class="wpb_single_image wpb_content_element vc_align_center">
				
				<figure class="wpb_wrapper vc_figure">
					<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="511" height="846" src="static/images/perso1.png" class="vc_single_image-img attachment-large" alt="" srcset="/static/images/perso1.png 511w, /static/images/perso1-250x414.png 250w, /static/images/perso1-120x199.png 120w, /static/images/perso1-325x538.png 325w, /static/images/perso1-100x165.png 100w, /static/images/perso1-199x330.png 199w, /static/images/perso1-173x286.png 173w, /static/images/perso1-345x572.png 345w, /static/images/perso1-111x183.png 111w, /static/images/perso1-221x366.png 221w, /static/images/perso1-211x349.png 211w, /static/images/perso1-422x698.png 422w, /static/images/perso1-181x300.png 181w" sizes="(max-width: 511px) 100vw, 511px" /></div><figcaption class="vc_figure-caption"><div class="bg-caption"></div><span class="caption-text">Yoga teacher</span></figcaption>
				</figure>
			</div>
			<div class="vc_empty_space"   style="height: 25px" ><span class="vc_empty_space_inner"></span></div>

			<div class="wpb_text_column wpb_content_element " >
				<div class="wpb_wrapper">
					<h5 style="text-align: center;"><span style="color: #c48ff5;">John Smith</span></h5>
					<p style="text-align: center;">I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

				</div>
			</div>
		</div></div></div><div class="wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp col-sm-3 col-6"><div class="vc_column-inner "><div class="wpb_wrapper">
			<div  class="wpb_single_image wpb_content_element vc_align_center">
				
				<figure class="wpb_wrapper vc_figure">
					<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="511" height="846" src="static/images/perso1.png" class="vc_single_image-img attachment-large" alt="" srcset="/static/images/perso1.png 511w, /static/images/perso1-250x414.png 250w, /static/images/perso1-120x199.png 120w, /static/images/perso1-325x538.png 325w, /static/images/perso1-100x165.png 100w, /static/images/perso1-199x330.png 199w, /static/images/perso1-173x286.png 173w, /static/images/perso1-345x572.png 345w, /static/images/perso1-111x183.png 111w, /static/images/perso1-221x366.png 221w, /static/images/perso1-211x349.png 211w, /static/images/perso1-422x698.png 422w, /static/images/perso1-181x300.png 181w" sizes="(max-width: 511px) 100vw, 511px" /></div><figcaption class="vc_figure-caption"><div class="bg-caption"></div><span class="caption-text">Yoga teacher</span></figcaption>
				</figure>
			</div>
			<div class="vc_empty_space"   style="height: 25px" ><span class="vc_empty_space_inner"></span></div>

			<div class="wpb_text_column wpb_content_element " >
				<div class="wpb_wrapper">
					<h5 style="text-align: center;"><span style="color: #c48ff5;">Jane Smith</span></h5>
					<p style="text-align: center;">I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

				</div>
			</div>
		</div></div></div></div></div></section></div><div class=" crop-wraper section-parent-wraper-rel "><div class="divider-section divider divider-top divider-white"><svg version="1.1" x="0px" y="0px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none">
			<path d="M0,0v24L240,0H0z"></path>
		</svg></div><div class="divider-section divider divider-bottom divider-white"><svg version="1.1" x="0px" y="0px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none">
			<path d="M240,24V0L0,24H240z"></path>
		</svg></div><section class="vc_custom_section my-small   img-vid-bg skew-top skew-bottom"><div class="container-fluid pt-big pb-big"><div data-vc-parallax="1.5" data-vc-parallax-image="/static/images/bg-1.jpg" class="row row-has-fill general parallax parallax-content-moving"><div class="col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper"><div 	class="vc_icon_element vc_icon_element-outer wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown vc_icon_element-align-center">
			<div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-xl vc_icon_element-style- vc_icon_element-background-color-grey">
				<span class="vc_icon_element-icon im im-newsletter" style="color:#c48ff5 !important"></span>	</div>
			</div>
			<div class="vc_empty_space"   style="height: 10px" ><span class="vc_empty_space_inner"></span></div>
			<h3 class=" title-mt-0 light h2 line l-down line-primary text-center  wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown">Newsletter</h3>
			<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp" >
				<div class="wpb_wrapper">
					<blockquote class="text-center"><p>
						<span style="color: #efefef;">I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</span>
					</p></blockquote>

				</div>
			</div>
			<div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
			<div class="text-center"><a href="#" class="btn btn-light arrow  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp">Subscribe now</a></div></div></div></div><div class="col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div></div></section></div><div class="  "><section class="vc_custom_section my-none  "><div class="container-fluid pt-medium pb-medium"><div class="row"><div class="wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper"><h3 class=" title-mt-0 pb-0 dark h2 line l-up line-primary text-center mb-sub ">Leave us a message !</h3><span class="subtitle h5 text-center ">Lorem ipsum dolor sit amet eras deus ex machina.</span><div class="vc_empty_space"   style="height: 25px" ><span class="vc_empty_space_inner"></span></div>
				<div role="form" class="wpcf7" id="wpcf7-f1672-p328-o1" lang="en-US" dir="ltr">
					<div class="screen-reader-response"></div>
					<form action="/#wpcf7-f1672-p328-o1" method="post" class="wpcf7-form" novalidate="novalidate">
						<div style="display: none;">
							<input type="hidden" name="_wpcf7" value="1672" />
							<input type="hidden" name="_wpcf7_version" value="5.0.3" />
							<input type="hidden" name="_wpcf7_locale" value="en_US" />
							<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1672-p328-o1" />
							<input type="hidden" name="_wpcf7_container_post" value="328" />
						</div>
						<div class="row">
							<div class="col-12">
								<div class="row padding-half">
									<div class="col-12 col-sm"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your Name (required)" /></span></div>
									<div class="col-12 col-sm"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your Email (required)" /></span></div>
								</div>
							</div>
							<div class="col-12">
								<span class="wpcf7-form-control-wrap menu-462"><select name="menu-462" class="wpcf7-form-control wpcf7-select" aria-invalid="false"><option value="Object 1">Object 1</option><option value="Object 2">Object 2</option><option value="Object 3">Object 3</option></select></span><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="6" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Your Message"></textarea></span>
							</div>
							<div class="col-12 text-center">
								<br><input type="submit" value="Send request" class="wpcf7-form-control wpcf7-submit" />
							</div>
						</div>
						<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div></div></div><div class="col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div></div></section></div><div class=" crop-wraper section-parent-wraper-rel "><div class="divider-section divider divider-top divider-white"><svg version="1.1" x="0px" y="0px" viewBox="0 0 240 24" enable-background="new 0 0 240 24" xml:space="preserve" preserveAspectRatio="none">
							<path d="M0,0v24L240,0H0z"></path>
						</svg></div><section class="vc_custom_section my-none  last-section mb--100  fullw overflow-hidden primary skew-top"><div class="container-fluid pt-none pb-none"><div class="row"><div class="wpb_animate_when_almost_visible wpb_fadeIn fadeIn col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="stt-gmap-module map_5e5a6c6a2355f " ><div id="map_5e5a6c6a2355f" class="map-holder"></div></div></div></div></div></div></div></section></div>

						<div class="clearfix"></div>
						
						
					</article>
					<!-- /article -->

				</main>

				@endsection