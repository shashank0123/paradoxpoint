<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\TrustProxies::class,
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \App\Http\Middleware\Language::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            \Spatie\Cors\Cors::class,
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'role' => \App\Http\Middleware\RoleMiddleware::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'web_installed' => \App\Http\Middleware\web_installed::class,
        'env' => \App\Http\Middleware\env::class,
        'admin' => \App\Http\Middleware\RedirectIfNotAdmin::class,
        'Customer' => \App\Http\Middleware\RedirectIfNotCustomer::class,
        'installer' => \App\Http\Middleware\Installation::class,
        'view_language' => \App\Http\Middleware\languages\view_language::class,
        'edit_language' => \App\Http\Middleware\languages\edit_language::class,
        'delete_language' => \App\Http\Middleware\languages\delete_language::class,
        'add_language' => \App\Http\Middleware\languages\add_language::class,
        'view_news' => \App\Http\Middleware\news\view_news::class,
        'edit_news' => \App\Http\Middleware\news\edit_news::class,
        'delete_news' => \App\Http\Middleware\news\delete_news::class,
        'add_news' => \App\Http\Middleware\news\add_news::class,
        'view_customer' => \App\Http\Middleware\customer\view_customer::class,
        'edit_customer' => \App\Http\Middleware\customer\edit_customer::class,
        'delete_customer' => \App\Http\Middleware\customer\delete_customer::class,
        'add_customer' => \App\Http\Middleware\customer\add_customer::class,
        'view_web_setting' => \App\Http\Middleware\web_setting\view_web_setting::class,
        'edit_web_setting' => \App\Http\Middleware\web_setting\edit_web_setting::class,
        'view_app_setting' => \App\Http\Middleware\app_setting\view_app_setting::class,
        'edit_app_setting' => \App\Http\Middleware\app_setting\edit_app_setting::class,
        'view_general_setting' => \App\Http\Middleware\general_setting\view_general_setting::class,
        'edit_general_setting' => \App\Http\Middleware\general_setting\edit_general_setting::class,
        'view_manage_admin' => \App\Http\Middleware\manage_admin\view_manage_admin::class,
        'edit_manage_admin' => \App\Http\Middleware\manage_admin\edit_manage_admin::class,
        'delete_manage_admin' => \App\Http\Middleware\manage_admin\delete_manage_admin::class,
        'add_manage_admin' => \App\Http\Middleware\manage_admin\add_manage_admin::class,
        'view_admin_type' => \App\Http\Middleware\admin_type\view_admin_type::class,
        'edit_admin_type' => \App\Http\Middleware\admin_type\edit_admin_type::class,
        'delete_admin_type' => \App\Http\Middleware\admin_type\delete_admin_type::class,
        'add_admin_type' => \App\Http\Middleware\admin_type\add_admin_type::class,
        'report' => \App\Http\Middleware\report\report::class,
        'dashboard' => \App\Http\Middleware\dashboard\dashboard::class,
        'manage_role' => \App\Http\Middleware\manage_role\manage_role::class,
        'view_vendor' => \App\Http\Middleware\vendors\view_vendor::class,
        'edit_vendor' => \App\Http\Middleware\vendors\edit_vendor::class,
        'delete_vendor' => \App\Http\Middleware\vendors\delete_vendor::class,
        'add_vendor' => \App\Http\Middleware\vendors\add_vendor::class,
        'cors' => \App\Http\Middleware\CORS::class,
        'edit_management' => \App\Http\Middleware\management\edit_management::class,
        'application_routes' => \App\Http\Middleware\app_setting\application_routes::class,
        'website_routes' => \App\Http\Middleware\web_setting\website_routes::class,
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * This forces non-global middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\Authenticate::class,
        \Illuminate\Routing\Middleware\ThrottleRequests::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Illuminate\Auth\Middleware\Authorize::class,
    ];
}
