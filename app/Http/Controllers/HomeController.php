<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Post;

class HomeController extends Controller
{
    public function index(Request $request): View
    {
        return view('home.index', [
            'posts' => Post::search($request->input('q'))
                             ->with('author', 'likes')
                             ->withCount('comments', 'thumbnail', 'likes')
                             ->latest()
                             ->paginate(20)
        ]);
    }
}
