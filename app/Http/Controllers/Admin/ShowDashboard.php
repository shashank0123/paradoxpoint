<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\View\View;
use Cache, Log;

class ShowDashboard extends Controller
{
    /**
     * Show the application admin dashboard.
     */
    // public function __invoke(): View
    // {
    //     return view('admin.dashboard.index', [
    //         'comments' =>  Comment::lastWeek()->get(),
    //         'posts' => Post::lastWeek()->get(),
    //         'users' => User::lastWeek()->get(),
    //     ]);
    // }


    public function __invoke(): View
    {
        $minutes = 60;
        $forecast = [];
        // $forecast = Cache::remember('forecast', $minutes, function () {
        // Log::info("Not from cache");
        // $app_id = config("here.app_id");
        // $app_code = config("here.app_code");
        // $lat = config("here.lat_default");
        // $lng = config("here.lng_default");
        // $url = "https://weather.api.here.com/weather/1.0/report.json?product=forecast_hourly&latitude=${lat}&longitude=${lng}&oneobservation=true&language=it&app_id=$app_id&apiKey=$app_code";
        // Log::info($url);
        // $client = new \GuzzleHttp\Client();
        // $res = $client->get($url);
        // if ($res->getStatusCode() == 200) {
        //   $j = $res->getBody();
        //   $obj = json_decode($j);
        //   $forecast = $obj->hourlyForecasts->forecastLocation;
        // }
        // return $forecast;
        // });

        // dd($forecast);
        return view('admin.admin.dashboard', [
            'comments' =>  Comment::lastWeek()->get(),
            'posts' => Post::lastWeek()->get(),
            'users' => User::lastWeek()->get(),
            "forecast" => $forecast,
        ]);
    }
}
