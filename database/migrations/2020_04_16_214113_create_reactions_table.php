<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('post_id')->nullable();
            $table->string('re_like')->default('0');
            $table->string('re_dislike')->default('0');
            $table->string('re_love')->default('0');
            $table->string('re_funny')->default('0');
            $table->string('re_angry')->default('0');
            $table->string('re_sad')->default('0');
            $table->string('re_wow')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reactions');
    }
}
