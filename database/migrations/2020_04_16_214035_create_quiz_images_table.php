<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image_default')->nullable();
            $table->string('image_small')->nullable();
            $table->string('file_name')->nullable();
            $table->string('image_mime')->default('jpg');
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_images');
    }
}
