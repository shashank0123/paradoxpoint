<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('role')->nullable();
            $table->string('role_name')->nullable();
            $table->string('admin_panel')->nullable();
            $table->string('add_post')->nullable();
            $table->string('manage_all_posts')->nullable();
            $table->string('navigation')->nullable();
            $table->string('pages')->nullable();
            $table->string('rss_feeds')->nullable();
            $table->string('categories')->nullable();
            $table->string('widgets')->nullable();
            $table->string('polls')->nullable();
            $table->string('gallery')->nullable();
            $table->string('comments_contact')->nullable();
            $table->string('newsletter')->nullable();
            $table->string('ad_spaces')->nullable();
            $table->string('users')->nullable();
            $table->string('seo_tools')->nullable();
            $table->string('settings')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles_permissions');
    }
}
