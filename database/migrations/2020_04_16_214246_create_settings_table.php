<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lang_id')->default('1');
            $table->string('site_title')->nullable();
            $table->string('home_title')->default('index');
            $table->string('site_description')->nullable();
            $table->string('keywords')->nullable();
            $table->string('application_name')->nullable();
            $table->string('primary_font')->default('1');
            $table->string('secondary_font')->default('2');
            $table->string('tertiary_font')->default('31');
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('pinterest_url')->nullable();
            $table->string('linkedin_url')->nullable();
            $table->string('bs_url')->nullable();
            $table->string('telegram_url')->nullable();
            $table->string('youtube_url')->nullable();
            $table->string('optional_url_button_name')->default('Click Here To See More');
            $table->text('about_footer')->nullable();
            $table->text('contact_text')->nullable();
            $table->text('contact_address')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->text('copyright')->nullable();
            $table->string('cookies_warning')->default('0');
            $table->text('cookies_warning_text')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
