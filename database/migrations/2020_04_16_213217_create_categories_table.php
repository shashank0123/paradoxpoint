<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lang_id')->default('1');
            $table->string('name')->nullable();
            $table->string('name_slug')->nullable();
            $table->string('parent_id')->default('0');
            $table->text('description')->nullable();
            $table->string('keywords')->nullable();
            $table->string('color')->nullable();
            $table->string('block_type')->nullable();
            $table->string('category_order')->default('0');
            $table->string('show_at_homepage')->default('1');
            $table->string('show_on_menu')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
