<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('site_lang')->default('1');
            $table->string('multilingual_system')->default('1');
            $table->string('show_hits')->default('1');
            $table->string('show_rss')->default('1');
            $table->string('show_newsticker')->default('1');
            $table->string('pagination_per_page')->default('10');
            $table->text('google_analytics')->nullable();
            $table->string('mail_library')->default('swift');
            $table->string('mail_protocol')->default('smtp');
            $table->string('mail_host')->nullable();
            $table->string('mail_port')->default('587');
            $table->string('mail_username')->nullable();
            $table->string('mail_password')->nullable();
            $table->string('mail_title')->nullable();
            $table->string('google_client_id')->nullable();
            $table->string('google_client_secret')->nullable();
            $table->string('bs_app_id')->nullable();
            $table->string('bs_secure_key')->nullable();
            $table->string('facebook_app_id')->nullable();
            $table->string('facebook_app_secret')->nullable();
            $table->string('facebook_comment')->nullable();
            $table->string('facebook_comment_active')->default('1');
            $table->string('show_featured_section')->default('1');
            $table->string('show_latest_posts')->default('1');
            $table->string('registeration_system')->default('1');
            $table->string('comment_system')->default('1');
            $table->string('comment_approval_system')->default('1');
            $table->string('show_post_author')->default('1');
            $table->string('show_post_date')->default('1');
            $table->string('menu_limit')->default('8');
            $table->text('custom_css_codes')->nullable();
            $table->text('custom_javascript_codes')->nullable();
            $table->text('adsense_activation_code')->nullable();
            $table->string('bs_key')->nullable();
            $table->string('purchase_code')->nullable();
            $table->string('recaptcha_site_key')->nullable();
            $table->string('recaptcha_lang')->nullable();
            $table->string('emoji_reactions')->default('1');
            $table->string('mail_contact_status')->default('0');
            $table->string('mail_contact')->nullable();
            $table->string('cache_system')->default('0');
            $table->string('cache_refresh_time')->default('1800');
            $table->string('refresh_cache_database_changes')->default('0');
            $table->string('email_verification')->default('0');
            $table->string('file_manager_show_files')->default('1');
            $table->string('audio_download_button')->default('1');
            $table->string('approve_added_user_post')->default('1');
            $table->string('approve_updated_user_post')->default('1');
            $table->string('timezone')->default('Asia/Kolkata');
            $table->string('sort_slider_post')->default('by_slider_order');
            $table->string('sort_featured_post')->default('by_featured_order');
            $table->string('newsletter')->default('1');
            $table->string('text_editor_lang')->default('en');
            $table->string('show_home_link')->default('1');
            $table->string('post_format_article')->default('1');
            $table->string('post_format_gallery')->default('1');
            $table->string('post_format_sorted_list')->default('1');
            $table->string('post_format_video')->default('1');
            $table->string('post_format_audio')->default('1');
            $table->string('post_format_trivia_quiz')->default('1');
            $table->string('post_format_personality_quiz')->default('1');
            $table->string('maintenance_mode_title')->default('Coming Soon');
            $table->string('maintenance_mode_description')->nullable();
            $table->string('maintenance_mode_status')->default('0');
            $table->string('sitemap_frequency')->default('monthly');
            $table->string('sitemap_last_modification')->default('server_response');
            $table->string('sitemap_priority')->default('automatically');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_settings');
    }
}
