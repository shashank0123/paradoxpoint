<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parent_id')->default('0');
            $table->string('post_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('email')->nullable();
            $table->string('name')->nullable();
            $table->text('comment')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('like_count')->default('0');
            $table->string('status')->default('1');          
            $table->datetime('posted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
