<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lang_id')->default('1');
            $table->text('title')->nullable();
            $table->text('slug')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->string('is_custom')->default('1');
            $table->string('page_default_name')->nullable();
            $table->text('page_content')->nullable();
            $table->string('page_order')->default('1');
            $table->string('visibility')->default('1');
            $table->string('title_active')->default('1');
            $table->string('breadcrumb_active')->default('1');
            $table->string('right_column_active')->default('1');
            $table->string('need_auth')->default('0');
            $table->string('location')->default('top');
            $table->string('link')->nullable();
            $table->string('parent_id')->default('0');
            $table->string('page_type')->default('page');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
