<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang_id')->default('1');
            $table->text('title')->nullable();
            $table->text('title_slug')->nullable();
            $table->text('title_hash')->nullable();
            $table->text('keywords')->nullable();
            $table->text('summary')->nullable();
            $table->text('content')->nullable();
            $table->string('category_id')->nullable();
            $table->string('image_big')->nullable();
            $table->string('image_default')->nullable();
            $table->string('image_slider')->nullable();
            $table->string('image_mid')->nullable();
            $table->string('image_small')->nullable();
            $table->string('image_mime')->default('jpg');
            $table->string('optional_url')->nullable();
            $table->string('pageviews')->default('0');
            $table->string('need_auth')->default('0');
            $table->string('is_slider')->default('0');
            $table->string('slider_order')->default('1');
            $table->string('is_featured')->default('0');
            $table->string('featured_order')->default('1');
            $table->string('is_recommended')->default('0');
            $table->string('is_breaking')->default('1');
            $table->string('is_scheduled')->default('0');
            $table->string('visibility')->default('1');
            $table->string('show_right_column')->default('1');
            $table->string('post_type')->default('post');
            $table->string('video_path')->nullable();
            $table->string('image_url')->nullable();
            $table->string('video_url')->nullable();
            $table->string('video_embed_code')->nullable();
            $table->string('user_id')->nullable();
            $table->string('status')->default('1');
            $table->string('feed_id')->nullable();
            $table->string('post_url')->nullable();
            $table->string('show_post_url')->default('1');
            $table->text('image_description')->nullable();
            $table->string('show_item_numbers')->default('1');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
