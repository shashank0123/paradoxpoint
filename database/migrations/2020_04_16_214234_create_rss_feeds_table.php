<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRssFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rss_feeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lang_id')->default('1');
            $table->string('feed_name')->nullable();
            $table->string('feed_url')->nullable();
            $table->string('post_limit')->nullable();
            $table->string('category_id')->nullable();
            $table->string('image_saving_method')->default('url');
            $table->string('auto_update')->default('1');
            $table->string('read_more_button')->default('1');
            $table->string('read_more_button_text')->default('Read More');
            $table->string('user_id')->nullable();
            $table->string('add_posts_as_draft')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rss_feeds');
    }
}
